module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // This enables dark mode support using a class
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
