export interface TimeRange {
  start: Date;
  end: Date;
}

export interface DataPoint {
  x: Date;
  y: number;
}
